import { T } from '../lib/index'

// todo: cover use cases where the expected input is of a wrong style

test('detectDecorator detects properly', () => {
	expect(T.decorator.detectDecorator('truth runner')).toEqual({"caps": false, "dash": false, "space": true, "under": false})
	expect(T.decorator.detectDecorator('truth-runner')).toEqual({"caps": false, "dash": true, "space": false, "under": false})
	expect(T.decorator.detectDecorator('truth_runner')).toEqual({"caps": false, "dash": false, "space": false, "under": true})
	expect(T.decorator.detectDecorator('truthRunner')).toEqual({"caps": true, "dash": false, "space": false, "under": false})
	expect(T.decorator.detectDecorator('truthRunner-and_other stuff')).toEqual({"caps": true, "dash": true, "space": true, "under": true})
})


test('toCamelCase decorates to "truthRunner"', () => {
	expect(T.decorator.toCamelCase('truth  runner')).toBe('truthRunner')
	expect(T.decorator.toCamelCase('truth runner')).toBe('truthRunner')
	expect(T.decorator.toCamelCase('truth runner rules')).toBe('truthRunnerRules')
	expect(T.decorator.toCamelCase('truth-runner')).toBe('truthRunner')
	expect(T.decorator.toCamelCase('truth—runner')).toBe('truthRunner')
	expect(T.decorator.toCamelCase('truth_runner')).toBe('truthRunner')
	expect(T.decorator.toCamelCase('TRUTH_RUNNER')).toBe('truthRunner')
})


test('camelToDash decorates to "truth-runner"', () => {
	expect(T.decorator.camelToDash('truthRunner')).toBe('truth-runner')
})


test('camelToSpace decorates to "truth runner', () => {
	expect(T.decorator.camelToSpace('truthRunner')).toBe('truth runner')
})


test('camelToUnder decorates to "truth_runner', () => {
	expect(T.decorator.camelToUnder('truthRunner')).toBe('truth_runner')
})


test('camelToUnderCaps decorates to "TRUTH_RUNNER', () => {
	expect(T.decorator.camelToUnderCaps('truthRunner')).toBe('TRUTH_RUNNER')
})

test('camelToRegular decorates to "Truth runner"', () => {
	expect(T.decorator.camelToRegular('truthRunner')).toBe('Truth runner')
	expect(T.decorator.camelToRegular('truthRunnerRules')).toBe('Truth runner rules')
})

test('camelToTitle decorates to "Truth Runner"', () => {
	expect(T.decorator.camelToTitle('truthRunner')).toBe('Truth Runner')
	expect(T.decorator.camelToTitle('truthRunnerRules')).toBe('Truth Runner Rules')
})

test('camelToCaps decorates to "TRUTH RUNNER"', () => {
	expect(T.decorator.camelToCaps('truthRunner')).toBe('TRUTH RUNNER')
	expect(T.decorator.camelToCaps('truthRunnerRules')).toBe('TRUTH RUNNER RULES')
})

test('dashToNull decorates to "truthrunner"', () => {
	expect(T.decorator.dashToNull('truth-runner')).toBe('truthrunner')
})


test('dashToSpace decorates to "truth runner"', () => {
	expect(T.decorator.dashToSpace('truth-runner')).toBe('truth runner')
})

test('toRegularForm decorates to "Truth runner"', () => {
	expect(T.decorator.toRegularForm('truth runner')).toBe('Truth runner')
})


test('inputPrefix decorates to "awesome-truth-runner"', () => {
	expect(T.decorator.inputPrefix('awesome', 'truthRunner')).toBe('awesome-truth-runner')
})