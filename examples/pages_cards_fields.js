let pages = {
	$name: {
		description: 'One sentence that explains the page',
		template: 'Base template to use for this page',
		route: 'canonical page, use :id e.g. to have an ID as a route component',
		scope: ['public', 'auth', 'private'], // array of one or more
		filters: ["functions that should show - or hide cards"],
		theme: ["generic, coin, userpref"],
		cards: ["array of cards to show on the card"]
	},
	...
}

let cards = {
	$name: {
		description: 'One sentence that explains the card',
		scope: ['public', 'auth', 'private'], // array of one or more
		cache: 1000, // how often (in milliseconds) this card updates
		filters: ["functions that should show - or hide data"],
		theme: ["generic, coin, userpref"],
		fields: ["array of fields to show on the card"],
		weight: 0, // determines render sequence starts at 0
		parentOf: [], // if it is a parent field, what children does it have
	},
	...
}

let fields = {
	$name: {
		description: 'One sentence that explains the card',
		icon: "description of icon",
		type: ["input-text, input-datetime, input-dropdown, boolean, integer, double, map, graph, etc."],
		qComponent: {}, // quasar specific component info incl. options etc.
		placeholder: 'placeholder value, image or description',
		validate: false, // regex to use for validation
		error: 'error text',
		test: 'test value',
		weight: 0, // determines render sequence starts at 0
		parentOf: [], // if it is a parent field, what children does it have
		resource: ["thirdparty api, cloudflare, redis, firestore, localStorage, ..."]
	},
	...
}
