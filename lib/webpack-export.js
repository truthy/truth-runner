/**
 * Truthy module for node.js and commonjs.
 * @module truth-runner
 * @exports Truthy
 */

const decorator = require('./decorator')




// We are following the webpack approach of exporting a class
module.exports = class Truthy {

	constructor (opts = {}) {
		this.opts = opts
	}
	apply (compiler) {
		compiler.hooks.emit.tapAsync('truth-runner', (compiler, callback) => {
			const options = this.opts[0].options || false


			if (preset === undefined || source === undefined) process.exit(1)

			truthRunner[preset](options)
			.catch((err) => {
				log(`Error in ${preset}`)
				throw new Error(err)
			})
			.then(() => {callback()})
		})
	}
}
