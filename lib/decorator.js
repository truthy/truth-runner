/**
 * @truthy decorator for node.js and commonjs.
 * @module decorator
 * @exports decorator
 */

module.exports = {}
let decorator = exports.decorator = {
	/**
	 * Naive detection to find presence of dash, underscore, space or caps
	 *
	 * @alias module:decorator.detectDecorator
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} detectDecorator('the-thing') => object
	 * @returns {object} e.g. {"caps": false, "dash": true, "space": false, "under": false}
	 */
	detectDecorator: ((str) => {
		const dash = /[-]/.test(str)
		const under = /[_]/.test(str)
		const space = /[ ]/.test(str)
		const caps = /[A-Z]/.test(str)
		return {caps, dash, space, under}
	}),
	/**
	 * Creates camelCase from a str
	 *
	 * @alias module:decorator.toCamelCase
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} toCamelCase('truth runner') => 'truthRunner'
	 * @returns {string}
	 */
	toCamelCase: ((str) => {
		return str.toLowerCase().replace(/[-_—]/g, ' ').replace(/(?:^\w|[A-Z]|\b\w)/g, (l, i) => (i === 0 ? l.toLowerCase() : l.toUpperCase())).replace(/\s+/g, '')
	}),
	/**
	 * Creates dashed from a camelCase str
	 *
	 * @alias module:decorator.camelToDash
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} camelToDash('truthRunner') => 'truth-runner'
	 * @returns {string}
	 */
	camelToDash: ((str) => {
		return str.replace(/\W+/g, '-').replace(/([a-z\d])([A-Z])/g, '$1-$2').toLowerCase()
	}),

	/**
	 * Creates spaced from a camelCase str
	 *
	 * @alias module:decorator.camelToSpace
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} camelToSpace('truthRunner') => 'truth runner'
	 * @returns {string}
	 */
	camelToSpace: ((str) => {
		return str.replace(/\W+/g, ' ').replace(/([a-z\d])([A-Z])/g, '$1 $2').toLowerCase()
	}),

	/**
	 * Creates lowercase underscored from a camelCase str
	 *
	 * @alias module:decorator.camelToUnder
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} camelToDash('truthRunner') => 'truth_runner'
	 * @returns {string}
	 */
	camelToUnder: ((str) => {
		return str.replace(/\W+/g, '_').replace(/([a-z\d])([A-Z])/g, '$1_$2').toLowerCase()
	}),

	/**
	 * Creates uppercase underscored from a camelCase str
	 *
	 * @alias module:decorator.camelToUnderCaps
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} camelToUnderCaps('truthRunner') => 'TRUTH_RUNNER'
	 * @returns {string}
	 */
	camelToUnderCaps: ((str) => {
		return str.replace(/\W+/g, '_').replace(/([a-z\d])([A-Z])/g, '$1_$2').toUpperCase()
	}),

	/**
	 * Creates first letter capitalized from a camelCase str
	 *
	 * @alias module:decorator.camelToRegular
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} camelToRegular('truthRunner') => 'Truth runner'
	 * @returns {string}
	 */
	camelToRegular: ((str) => {
		return str.replace(/\W+/g, ' ').replace(/([a-z\d])([A-Z])/g, '$1 $2').toLowerCase().replace(/^./, (ltr => ltr.toUpperCase()))
	}),

	/**
	 * Capitalizes the first letter of all words
	 *
	 * @alias module:decorator.camelToTitle
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} camelToTitle('truthRunner') => 'Truth Runner'
	 * @returns {string}
	 */
	camelToTitle: ((str) => {
		return str.replace(/\W+/g, ' ').replace(/([a-z\d])([A-Z])/g, '$1 $2').replace(/^./, (ltr => ltr.toUpperCase()))
	}),

	/**
	 * Capitalizes all letters
	 *
	 * @alias module:decorator.camelToCaps
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} camelToCaps('truthRunner') => 'TRUTH RUNNER'
	 * @returns {string}
	 */
	camelToCaps: ((str) => {
		return str.replace(/\W+/g, ' ').replace(/([a-z\d])([A-Z])/g, '$1 $2').toUpperCase()
	}),
	/**
	 * Removes dash from a string
	 *
	 * @alias module:decorator.dashToNull
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} hyphenToNull('the-thing') => 'thething'
	 * @returns {string}
	 */
	dashToNull: ((str) => {
		return str.replace(/-/g, '')
	}),

	/**
	 * Turns dash to a space
	 *
	 * @alias module:decorator.dashToSpace
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} hyphenToSpace('the-thing') => 'the thing'
	 * @returns {string}
	 */
	dashToSpace: ((str) => {
		return str.replace(/-/g, ' ')
	}),

	/**
	 * Capitalise first letter of string
	 *
	 * @alias module:decorator.toRegularForm
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} toRegularForm('the-thing') => 'The thing'
	 * @returns {string}
	 */
	toRegularForm: ((str) => {
		return str.replace(/([A-Z])/g, ' $1')
		.replace(/^./, (ltr => ltr.toUpperCase()))
	}),
	/**
	 * Prefix input with dash style
	 *
	 * @alias module:decorator.inputPrefix
	 * @param {string} pre - a prefix string to add
	 * @param {string} str - a src string to be decorated
	 * @example {@lang es6} inputPrefix('prefix','theThing') => ' prefix-the-thing'
	 * @returns {string}
	 */
	inputPrefix: ((pre, str) => {
		return `${pre}-${decorator.camelToDash(str)}`
	})
}

/* istanbul ignore next */
if (typeof exports !== 'undefined') {
	/* istanbul ignore next */
	if (typeof module !== 'undefined' && module.exports) {
		exports = module.exports = decorator
	}
	exports.decorator = decorator
}
