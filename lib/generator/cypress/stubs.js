module.exports = {}
let stubs = exports.stubs = {
	vuex: {
		/**
		 * Test that a store key:value pair exists
		 *
		 * @alias module:truthy.tests.cypress.vuex
		 * @param {string} storePath - a key to test for store presence
		 * @param {string} keys - a value that the key must equal
		 * @example {@lang es6} tests.cypress.store('fullName', 'John Doe')  => true
		 */
		vuexExist: (storePath2, keys) => {
			it('TEST:VUEX EXIST', () => {
				cy.log(storePath2)
				getStore()
				.its(storePath2)
				.should('have.keys', keys)
			})
		},
		/**
		 * Test that a store key:value pair exists
		 *
		 * @alias module:truthy.tests.cypress.vuex
		 * @param {string} keyPath - a key to test for store presence
		 * @param {string} value - a value that the key must equal
		 * @example {@lang es6} tests.cypress.store('fullName', 'John Doe')  => true
		 */
		vuexCompare: (keyPath, value) => {
			it('TEST:VUEX COMPARE', () => {
				getStore().its(keyPath).should('eq', value)
			})
		}
	},
	localStorage: {

		/**
		 * Test that a store key:value pair exists
		 *
		 * @alias module:truthy.tests.cypress.vuex
		 * @param {string} key - a key to test for store presence
		 * @param {string} value - a value that the key must equal
		 * @param {string} object - an entire object to look at
		 * @example {@lang es6} tests.cypress.store('fullName', 'John Doe')  => true
		 */
		write: (key, value) => {
			it('TEST:LOCAL_STORAGE', () => {
				cy.should(() => {
					expect(testStorage(key)).to.eq(value);
				});
			});
		},
		/**
		 * Test that a store key:value pair exists
		 *
		 * @alias module:truthy.tests.cypress.vuex
		 * @param {string} key - a key to test for store presence
		 * @param {string} value - a value that the key must equal
		 * @param {string} object - an entire object to look at
		 * @example {@lang es6} tests.cypress.store('fullName', 'John Doe')  => true
		 */
		read: (key, value) => {
			it('TEST:LOCAL_STORAGE', () => {
				cy.should(() => {
					expect(testStorage(key)).to.eq(value);
				});
			});
		},
		/**
		 * Test that a store key:value pair exists
		 *
		 * @alias module:truthy.tests.cypress.vuex
		 * @param {string} key - a key to test for store presence
		 * @param {string} value - a value that the key must equal
		 * @param {string} object - an entire object to look at
		 * @example {@lang es6} tests.cypress.store('fullName', 'John Doe')  => true
		 */
		clear: (key, value) => {
			it('TEST:LOCAL_STORAGE', () => {
				cy.should(() => {
					expect(testStorage(key)).to.eq(value);
				});
			});
		},
	},
	cookie: {
		/**
		 * Test that a store key:value pair exists
		 *
		 * @alias module:truthy.tests.cypress.vuex
		 * @param {string} key - a key to test for store presence
		 * @param {string} value - a value that the key must equal
		 * @param {string} object - an entire object to look at
		 * @example {@lang es6} tests.cypress.store('fullName', 'John Doe')  => true
		 */
		write: (key, value) => {
			it('TEST:LOCAL_STORAGE', () => {
				cy.should(() => {
					expect(testStorage(key)).to.eq(value);
				});
			});
		},
		/**
		 * Test that a store key:value pair exists
		 *
		 * @alias module:truthy.tests.cypress.vuex
		 * @param {string} key - a key to test for store presence
		 * @param {string} value - a value that the key must equal
		 * @param {string} object - an entire object to look at
		 * @example {@lang es6} tests.cypress.store('fullName', 'John Doe')  => true
		 */
		read: (key, value) => {
			it('TEST:LOCAL_STORAGE', () => {
				cy.should(() => {
					expect(testStorage(key)).to.eq(value);
				});
			});
		},
		/**
		 * Test that a store key:value pair exists
		 *
		 * @alias module:truthy.tests.cypress.vuex
		 * @param {string} key - a key to test for store presence
		 * @param {string} value - a value that the key must equal
		 * @param {string} object - an entire object to look at
		 * @example {@lang es6} tests.cypress.store('fullName', 'John Doe')  => true
		 */
		clear: (key, value) => {
			it('TEST:LOCAL_STORAGE', () => {
				cy.should(() => {
					expect(testStorage(key)).to.eq(value)
				})
			})
		},
	},
	quasarInputs: {
		validation: ((obj) => {
			cy.log('VALIDATION')
		}),
		/**
		 * Test that a text input type works
		 *
		 * @alias module:T.tests.cypress.text
		 * @param {array} obj - array of objects that uses key, reg and win
		 * @returns {function}
		 */
		text: ((obj) => {
			cy.log('text');
			if (obj.window.res.inputs[obj.input].multiple) {
				/* eslint-disable-next-line */
				for (let x = 1; x <= obj.window.res.inputs[obj.input].count; x++) {
					cy.get(`input[data-cy=input-${T.decorator.camelToDash(obj.input)}-${x}]`)
					.type(`${obj.registration[obj.step][obj.input + x]}`)
					.should('have.value', obj.registration[obj.step][obj.input + x]);
				}
			} else {
				cy.get(`input[data-cy=input-${T.decorator.camelToDash(obj.input)}]`)
				.type(`${obj.registration[obj.step][obj.input]}`)
				.should('have.value', obj.registration[obj.step][obj.input]);
			}
		}),
		list: ((obj) => {
			cy.log('text');
			/* eslint-disable-next-line */
			for (let x = 0; x + 1 <= obj.registration[obj.step][obj.input].length; x++) {
				cy.get(`[data-cy=list-${T.decorator.camelToDash(obj.input)}]`).within(() => {
					cy.get('input')
					.first()
					.type(`${obj.registration[obj.step][obj.input][x]}{enter}`)
					.wait(100);
				});
			}
		}),
		textarea: ((obj) => {
			cy.log('textarea');
			cy.get(`input[data-cy=input-${T.decorator.camelToDash(obj.input)}]`)
			.focus()
			.type(`${obj.registration[obj.step][obj.input]}`)
			.should('have.value', obj.registration[obj.step][obj.input]);
		}),
		date: ((obj) => {
			cy.log('date');
			cy.get(`input[data-cy=input-${T.decorator.camelToDash(obj.input)}]`)
			.focus()
			.type(`${obj.registration[obj.step][obj.input]}`)
			.should('have.value', obj.registration[obj.step][obj.input]);
		}),
		select: ((obj) => {
			cy.log('select');
			cy.get(`[data-cy=select-${T.decorator.camelToDash(obj.input)}]`)
			.click({ force: true, multiple: true })
			.wait(500);
			cy.get('.q-popover')
			.contains(obj.registration[obj.step][obj.input])
			.click();
		}),
		grid: ((obj) => {
			// todo: be careful about the step!!!
			cy.log('grid');
			cy.get('[data-cy=grid-checkbox')
			.click({ force: true, multiple: true });
		}),
		radio: ((obj) => {
			cy.log('radio');
			cy.get(`[data-cy=input-${T.decorator.camelToDash(obj.input)}]`)
			.contains(obj.registration[obj.step][obj.input])
			.click();
			cy.log('check vuex store');
		}),
		checkbox: ((obj) => {
			cy.log('checkbox');
			cy.get(`[data-cy=input-${T.decorator.camelToDash(obj.input)}]`)
			.contains(obj.window.res.inputs[obj.input].label)
			.click({ force: true, multiple: true });
		}),
		option: ((obj) => {
			cy.log('option');
			cy.get(`[data-cy=option-${T.decorator.camelToDash(obj.input)}]`).within(() => {
				cy.get('.q-checkbox')
				.click({ force: true, multiple: true });
			});
		}),
		yesno: ((obj) => {
			cy.log('yesno');
			cy.get(`[data-cy=yesno-${T.decorator.camelToDash(obj.input)}]`)
			.click();
		}),
	}
}
/* istanbul ignore next */
if (typeof exports !== 'undefined') {
	/* istanbul ignore next */
	if (typeof module !== 'undefined' && module.exports) {
		exports = module.exports = stubs
	}
	exports.decorator = stubs
}
