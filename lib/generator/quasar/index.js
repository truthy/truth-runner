
/**
 * @truthy generator for quasar
 * @module generator.quasar
 * @exports quasar
 */

const R = require('ramda')
const compromise = require('compromise')
const T = require('../../index.js')

module.exports = {}
let quasar = exports.quasar = {
	init: (() => {

	}),
	store: (() => {

	}),
	routes: (() => {

	}),
	pwa: (() => {

	}),
	electron: (() => {

	}),
	cordova: (() => {

	}),
	testing: (() => {

	})
}

/* istanbul ignore next */
if (typeof exports !== 'undefined') {
	/* istanbul ignore next */
	if (typeof module !== 'undefined' && module.exports) {
		exports = module.exports = quasar
	}
	exports.quasar = quasar
}
