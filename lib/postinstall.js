#!/usr/bin/env node

const fs = require('fs'),
			fse = require('fs-extra'),
	    path = require('path'),
	    resolve = path.resolve,
	    join = path.join,
		  tgt = 'truthy'

function getAppDir () {
	let dir = process.cwd()

	while (dir.length > 1) {
		if (fs.existsSync(path.join(dir, 'quasar.conf.js'))) {
			return dir
		}
		dir = path.normalize(path.join(dir, '..'))
	}
}

const
	appDir = getAppDir(),
	cliDir = resolve(__dirname, '../..'),
	appPaths = {
		resolve: {
			cli: dir => join(cliDir, dir)
		}
	}

fse.copySync(appPaths.resolve.cli('templates/truthy'), appDir + '/' + tgt)
fse.copySync(appPaths.resolve.cli('templates/truthy.conf.yml'), appDir + '/truthy.conf.yml')