/**
 * Truthy module for node.js and commonjs.
 * @module truth-runner
 * @exports T
 */

const decorator = require('./decorator')
const cypress = require('./generator/cypress')
const version = require('../package.json').version

module.exports = {}
let T = exports.T = {
	decorator,
	cypress,
	version
}

/* istanbul ignore next */
if (typeof exports !== 'undefined') {
	/* istanbul ignore next */
	if (typeof module !== 'undefined' && module.exports) {
		exports = module.exports = T
	}
	exports.T = T
}