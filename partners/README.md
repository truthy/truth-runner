# @truthy supportership

**@truthy** is an open-source project that stands on the shoulders of giants like Ramda, Babel, Typescript, Webpack, Vue.js, Quasar, Jest and Cypress. It is licensed as a GPL-3 project for open-source redistribution and can also be directly licensed from its author for commercial purposes. I lay no claim to the ownership (or fitness) of the artifacts that it creates, as the results of your work are exclusively yours.

Supporting the **@truthy** project can take a variety of forms - depending on the profile and needs of the supporter. The following text will hopefully explain the ways that we are able to work with external parties. However, if you or your company have additional ideas, please feel free to get in touch.

## TYPES OF SUPPORTERSHIP
``` 
IDEAL
- Contributions
- Donations
- Pure Sponsoring

PARTNERSHIP
- Strategic Partners
- Cross-Marketing
- Technological Integration Partner

COMMERCIAL
- Feature Contract
- Integration Consulting
- Licensing for Redistribution
``` 

## IDEAL 
### Contributions
From reporting issues to submitting service generators, contributing is a great way to get involved. I use the “issue, fork and PR against dev branch” approach, and require contributors both to sign off and agree to the CLA. Writing code and making pull-requests is a great way to champion approaches that matter to you. In order to participate in paid bounty challenges, you must have already made at least two approved PR’s.

### Donations
At this time, **@truthy** is not a foundation or registered nonprofit. If you wish to give back to the community by offsetting your operational income with a tax-exempt donation, please consider making a donation in **@truthy**‘s name to the Free Software Foundation if in the US or NLNET if you are in the EU.

### Pure Sponsoring
A “pure” sponsorship is a no-strings-attached gift or transfer of money-like assets. Unless explicitly requested, all sponsors receive their name or registered DBA listed on the website and repository donor-board. Different levels entitle the sponsor to varying visibility of the embedding of their visual brand identity. All sponsors receive access to a “Patreon’s only” help channel.

- Diamond Level (5, +1000€ / month) Logo 512x512, link
- Ruby Level (10, 500-999€ / month) Logo 256x256, link
- Quartz Level (50, 100-499€ / month) Logo 128x128, link
- Salt Level (Unlimited, 10-99€ / month) Name, link

## PARTNERSHIP
### Strategic partners
There are different levels of sponsorship, and sponsors can choose to be associated with one specific scope of the project that has particular relevance to their interests. These partnerships are by invitation only, are contractual and for terms no less than six months. 
- Lonsdaleite Level => Main Partner (1, by invitation only): prominent appearance in general and thematic scopes of all official media assets, such as websites, blogs, newsletters, slide-decks, press-releases, books etc. 
- Moissanite Level => Thematic Partner (5, by invitation only): prominent appearance as above, however within thematic scope, visually “above & before” crystalline sponsors, “underneath & after” main partner.

### Cross-Marketing
Cross-marketing partnerships focus on the distribution of common messages using medial techniques for extending each partner’s reach to the audiences of the other partner, and can include guest posts on respective blogs, panel discussions etc. Depending on capacity, this type of marketing is generally available to all sponsors.

### Technological Integration Partnership
This type of partnership involves the deep integration of external software and/or platforms into the core offering of truthy. All such extensions must :
- Be entirely optional
- Include a viably useful amount of free service to the end-users of truthy

## COMMERCIAL
### Feature Contract
Companies desiring additional features can contract the **@truthy** development team to implement bespoke features for specific scenarios. Please contact me to discuss this type of support.

### Integration Consulting
If you would like technical advice regarding the integration of **@truthy** in a NEW project, by all means get in touch! Although I generally prefer not to get involved in your code base, you can retain my services in the planning phases.

### Licensing for Redistribution
If for some reason you would like to redistribute **@truthy** as part of a proprietary project, the GPL-3 license permits it. However, if you need to make changes to the code-base and your business model does not permit releasing your code to the community, I am prepared to offer a very expensive licensing agreement. 50% of this fee (about what is left after taxes and overhead) will be donated to the Linux Foundation.
